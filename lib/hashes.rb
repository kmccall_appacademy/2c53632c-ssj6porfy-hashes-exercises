# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length_hash = {}
  words = str.split(' ')
  words.each { |word| word_length_hash[word] = word.length }
  word_length_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.select { |key, value| value == hash.values.max }.first[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |key, value| older[key] = value }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = word.chars.uniq
  letter_count_hash = {}
  letters.each { |letter| letter_count_hash[letter] = word.count(letter) }
  letter_count_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  new_hash = {} # setting the default value to 0
  arr.each { |el| new_hash[el] = 0 } # Without using .uniq, creating keys in new_hash with a value of 0
  new_hash.keys # returns keys array. The keys are uniq since hashes don't allow keys to be assigned multiple values.
end

# Define a method that, given an array of numbers, returns a hash with 'even'
# and 'odd' as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_count_hash = Hash.new(0)
  numbers.each do |number|
    even_odd_count_hash['odd'.to_sym] += 1 if number.odd?
    even_odd_count_hash['even'.to_sym] += 1 if number.even?
  end
  even_odd_count_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count_hash = Hash.new(0)
  string.each_char do |char|
    case char
    when 'a' then vowel_count_hash[char] += 1
    when 'e' then vowel_count_hash[char] += 1
    when 'i' then vowel_count_hash[char] += 1
    when 'o' then vowel_count_hash[char] += 1
    when 'u' then vowel_count_hash[char] += 1
    end
  end
  most_common_hash = vowel_count_hash.select do |key, value|
    value == vowel_count_hash.values.max
  end
  most_common_hash.sort_by { |key, value| key }.first[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { 'Asher' => 6, 'Bertie' => 11,
# 'Dottie' => 8, 'Warren' => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ['Bertie', 'Dottie'],
# ['Bertie', 'Warren'], ['Dottie', 'Warren'] ]
def fall_and_winter_birthdays(students)
  selected_students = students.select { |key, value| value >= 7 }
  selected_students.keys.combination(2).to_a # creates all combinations of students
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(['cat',
# 'cat', 'cat']) => 1 biodiversity_index(['cat', 'leopard-spotted ferret',
# 'dog']) => 9
def biodiversity_index(specimens)
  specimens_hash = Hash.new(0)
  specimens.each { |specimen| specimens_hash[specimen] += 1 }
  smallest_population_size = specimens_hash.values.min
  largest_population_size = specimens_hash.values.max
  number_of_species = specimens_hash.keys.size

  number_of_species**2 * (smallest_population_size / largest_population_size)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign('We're having a yellow ferret sale for a good cause over at the
# pet shop!', 'Leopard ferrets forever yo') => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_hash = character_count(normal_sign)
  vandalized_hash = character_count(vandalized_sign)
  vandalized_hash.all? { |key, value| normal_hash[key] >= vandalized_hash[key] }
end

def character_count(str)
  character_count_hash = Hash.new(0)
  letters = str.downcase
  letters.each_char { |letter| character_count_hash[letter] += 1 }
  character_count_hash
end
